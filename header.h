#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <stack>
#include "Header.h"
#include <cstddef>        // std::size_t

using namespace std;
typedef void(*PF)(stack <double> &st, map <string, double> &Mapconst, const vector <string> &Myparametr);
int MySplit(const string &Cstr, vector <string> &words);
void createMap(map <string, PF> &MyMap);

class MyExep 
{
public:
	MyExep(const string &s2)
	{
		errorMessage = s2;
	}
	
	string errorMessage;
};

class MyCalcExep : public MyExep
{
public:
	MyCalcExep(const string &s1, const string &s2): MyExep(s2)
	{
		commandName = s1;
	}

	string commandName;

};

